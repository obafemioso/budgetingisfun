import { BudgetItem } from './budgetItem';

export class Budget {
  name: string;
  items: BudgetItem[];
}