import { Component } from '@angular/core';

@Component({
    selector: 'my-app',
    template: `
      <h1>{{title}}</h1>
      <nav>
        <a routerLink="/budgets" routerLinkActive="active">Budgets</a>
      </nav>
      <router-outlet></router-outlet>
    `
})
export class AppComponent { 
  title = "Budgetting is Fun";
}
