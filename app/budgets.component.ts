import { Component, OnInit } from '@angular/core';

import { Budget }        from './budget';
import { BudgetService } from './budget.service';

@Component({
  moduleId: module.id,
  selector: 'my-budgets',
  templateUrl: 'budgets.component.html'
})

export class BudgetsComponent implements OnInit {
  budgets: Budget[] = [];

  constructor(
    private budgetService: BudgetService
  ) { }

  getBudgets(): void {
    this.budgetService.getBudgets().then(
      budgets => this.budgets = budgets);
  }

  ngOnInit(): void {
    this.getBudgets();
  }
}