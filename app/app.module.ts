import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule }    from '@angular/http';
import { routing }       from './app.routing';
import { AngularFireModule } from 'angularfire2';

import { AppComponent }    from './app.component';
import { BudgetsComponent} from './budgets.component';

import { BudgetService } from './budget.service';

export const firebaseConfig = {
  apiKey: 'AIzaSyCOqxyqJWHbVXQoRe5yeDDckDFaezCWGFQ',
  authDomain: 'project-5329216778150346102.firebaseapp.com',
  databaseURL: 'https://project-5329216778150346102.firebaseio.com',
  storageBucket: 'project-5329216778150346102.appspot.com'
};

@NgModule({
  imports: [ 
    BrowserModule,
    HttpModule,
    AngularFireModule.initializeApp(firebaseConfig),
    routing
   ],
  declarations: [ 
    AppComponent,
    BudgetsComponent
  ],
  providers: [
    BudgetService
  ],
  bootstrap: [ AppComponent ]
})

export class AppModule { }
