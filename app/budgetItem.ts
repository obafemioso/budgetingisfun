export class BudgetItem {
  name: string;
  amount: number;
}