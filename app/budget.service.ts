import { Injectable }    from '@angular/core';
import { AngularFire, FirebaseListObservable } from 'angularfire2';

import { Budget } from './budget';

@Injectable()
export class BudgetService {

  constructor (
    private af: AngularFire
  ) { }

  getBudgets(): Promise<Budget[]> {
    console.log('getting budgets');
    return null;
  }

  private handleError(error: any): void {
    console.error('An error occurred', error);
    // return Promise.reject(error.message || error);
  }
}