import { ModuleWithProviders }    from '@angular/core';
import { Routes, RouterModule }   from '@angular/router';

import { BudgetsComponent }        from './budgets.component';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '/budgets',
    pathMatch: 'full'
  },
  {
    path: 'budgets',
    component: BudgetsComponent
  }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);